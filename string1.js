

function string1(string) {
    let new_string;
    let is_negative_string;

    if(string[0]=="-"){
        is_negative_string = true;
        new_string = string.slice(1);
    }
    else{
        new_string = string;
    }
    let result = "";
    let num_of_dot = 0;
    for(let i=0;i<new_string.length;i++){
        if(new_string[i]=="."){
            num_of_dot += 1;
        }
    }
    
    for(let i=1;i<new_string.length;i++){
        if((Number.isFinite(Number(new_string[i])) || new_string[i]=="." ) && num_of_dot<2){
            result += (new_string[i]);
        }else if(new_string[i]!=","){
            return 0
        }
    }
    if(is_negative_string){
        result = "-" + result;
    }
    
    return Number(result);
}

module.exports = string1;