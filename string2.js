

function string2(test_string) {
    let new_array = test_string.split(".");
    if(new_array.length==4){
        for(let i=0;i<new_array.length;i++){
            if(/\D/.test(new_array[i]) || new_array[i]>255 || new_array[i]<0){
                return []
            }
        }
    }
    return new_array;
}

module.exports = string2;