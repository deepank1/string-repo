

function string4(test_object) {
    let full_name = "";
    if(typeof(test_object)=="object"){
        for(let key in test_object){
            let temp_case = test_object[key].toLowerCase()
            
            let title_case = temp_case[0].toUpperCase() + temp_case.slice(1)

            full_name += title_case + " "
            
        }
        return full_name.trim();
    }
    return 0;
}

module.exports = string4;