
function string3(test_string) {
    
    let array_of_date_element = test_string.split("/");
    
    for(let i=0;i<array_of_date_element.length;i++){
        if(/\D/.test(array_of_date_element[i])){
            return 0;
        }
    }
    
    let month = array_of_date_element[1];
    if(month>12){
        return 0
    }
    return month;
}

module.exports = string3;
